import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CallableExampleThree {

	public static void main(String[] args) throws Exception {
	
		Callable<Integer> task1=()->{
			int sum=0;
			for(int i=1;i<=10;i++) {
				sum=sum+i;
			}
			return sum;
		};
		Callable<Integer> task2=()->{
			int sum=0;
			for(int i=11;i<=20;i++) {
				sum=sum+i;
			}
			return sum;
		};
		Callable<Integer> task3=()->{
			int sum=0;
			for(int i=21;i<=30;i++) {
				sum=sum+i;
			}
			return sum;
		};
		
		List<Callable<Integer>> tasks=Arrays.asList(task1,task2,task3);
		
		ExecutorService service=Executors.newFixedThreadPool(3);
		
		int result=service.invokeAny(tasks);
		
		System.out.println("Result : "+result);
		
		service.shutdown();
	}

}
