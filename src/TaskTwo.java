
public class TaskTwo extends Thread {
	public void run() {
		// task-2 (to print 11 to 20)
		String name=Thread.currentThread().getName();
		for (int i = 11; i <= 20; i++) {
			System.out.println(name + " => " + i);
		}
	}
}
