class One implements Runnable{
	public void run() {
		int sum=0;
		for(int i=1; i<=10; i++) {
			int n=2*i;
			System.out.println("2 x "+i+" = "+n);
			sum=sum+n;
		}
		System.out.println("Sum : "+sum);
	}
}
class Two implements Runnable{
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.println("3 x "+i+" = "+(3*i));
		}
		
	}
}
class Three implements Runnable{
	public void run() {
		for(int i=1; i<=10; i++) {
			System.out.println("4 x "+i+" = "+(4*i));
		}

	}
}
public class Test {

	public static void main(String[] args) {
		
		Thread t1=new Thread(new One());
		Thread t2=new Thread(new Two());
		Thread t3=new Thread(new Three());
		t1.start();
		t2.start();
		t3.start();

	}

}
