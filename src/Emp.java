import java.util.Scanner;

public class Emp extends Thread {
	private int eno;
	private int salary;
	
	public void run() {
		processSalary();
	}
	public Emp(int eno, int salary) {
		this.eno=eno; this.salary=salary;
	}
	public void processSalary() {
		String name=Thread.currentThread().getName();
		System.out.println(name+" =>Basic of "+eno+" : "+salary);
		if(eno==112) {
			Scanner sc=new Scanner(System.in);
			int n=sc.nextInt();
		}
		System.out.println(name+" =>HRA of   "+eno+" : "+salary*20/100);
		System.out.println(name+" =>CA  of   "+eno+" : "+salary*10/100);
		System.out.println(name+" =>MA  of   "+eno+" : "+salary*5/100);
		
	}
	
	public static void main(String args[]) {
		Emp e1=new Emp(111,10000);
		Emp e2=new Emp(112,20000);
		Emp e3=new Emp(113,30000);
		e1.start();
		e2.start();
		e3.start();
	}
}
