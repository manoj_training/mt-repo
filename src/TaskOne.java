
public class TaskOne extends Thread {
	public void run() {
		// task-1 (to print 1 to 10)
		String name=Thread.currentThread().getName();
		for (int i = 1; i <= 10; i++) {
			System.out.println(name + " => " + i);
		}
	}
}
