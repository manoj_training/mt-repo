
public class DoubtOne {

	public static void main(String[] args) {
		
		Runnable r1=new Runnable() {
			public void run() {
				System.out.println("Task 1 Completed By "+Thread.currentThread().getName());
			}
		};
		
		Runnable r2=new Runnable() {
			public void run() {
				System.out.println("Task 2 Completed By "+Thread.currentThread().getName());
			}
		};
		
		Thread t1=new Thread(r1); t1.setName("First-Thread");
		Thread t2=new Thread(r2); t2.setName("Second-Thread");
		
		//t1.run();
		//t2.run();
		t1.start();
		t2.start();

	}

}
