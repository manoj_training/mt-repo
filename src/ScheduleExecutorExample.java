import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ScheduleExecutorExample {

	public static void main(String[] args) {
		
		Runnable task=()->System.out.println(new java.util.Date());

		ScheduledExecutorService service=Executors.newSingleThreadScheduledExecutor();
		//ExecutorService service=Executors.newSingleThreadExecutor();
		//service.submit(task);
		//service.schedule(task, 60, TimeUnit.SECONDS);
		System.out.println("Submitting Task At : "+new java.util.Date());
		service.scheduleAtFixedRate(task, 30, 20, TimeUnit.SECONDS);
		//service.shutdown();
	}

}
