import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableExample {
	public static void main(String[] args) throws Exception {
		// suppose we want to add the numbers from 1 to 50 and want the result back.

		Callable<Integer> callable=()->{
				int sum=0;
				for(int i=1; i<=10; i++) {
					sum=sum+i;
				}
				Thread.sleep(25000);
				return sum;
		};

		ExecutorService service=Executors.newSingleThreadExecutor();
		System.out.println("Submitted The Task....");
		//int result=service.submit(callable).get();
		Future<Integer> future=service.submit(callable);
		System.out.println("Doing Something Else....");
		System.out.println("Doing Something Else....");
		System.out.println("Now Asking For Result....");
		int result=future.get();
		System.out.println("Result :  "+result);
		service.shutdown();
		
		
		
	}
}
