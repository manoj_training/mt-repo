
public class TaskThree extends Thread {
	public void run() {
		// task-3 (to print 21 to 30)
		String name=Thread.currentThread().getName();
		for (int i = 21; i <= 30; i++) {
			System.out.println(name + " => " + i);
		}
	}
}
