import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ExecutorExampleOne {

	public static void main(String[] args) throws Exception {

		Runnable task1 = new Runnable() {
			public void run() {
				String name=Thread.currentThread().getName();
				for (int i = 1; i <= 10; i++) {
					System.out.println(name+"=>2 x " + i + " = " + (2*i));
				}
			}
		};
		Runnable task2 = new Runnable() {
			public void run() {
				String name=Thread.currentThread().getName();
				for (int i = 1; i <= 10; i++) {
					System.out.println(name+"=>3 x " + i + " = " + (3*i));
				}
			}
		};
		Runnable task3 = new Runnable() {
			public void run() {
				String name=Thread.currentThread().getName();
				for (int i = 1; i <= 10; i++) {
					System.out.println(name+"=>4 x " + i + " = " + (4*i));
				}
			}
		};
		
		Runnable task4 = new Runnable() {
			public void run() {
				String name=Thread.currentThread().getName();
				for (int i = 1; i <= 10; i++) {
					System.out.println(name+"=>5 x " + i + " = " + (5*i));
				}
			}
		};
		ExecutorService service=Executors.newCachedThreadPool();
		//ExecutorService service=Executors.newFixedThreadPool(2);
		//ExecutorService service=Executors.newSingleThreadExecutor();
		service.submit(task1);
		service.submit(task2);
		service.submit(task3);
		Thread.sleep(70000);
		service.submit(task4);
		
	
		service.shutdown();
		
		
	}

}
